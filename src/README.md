**WebRTC is a free, open software project** that provides browsers and mobile
applications with Real-Time Communications (RTC) capabilities via simple APIs.
The WebRTC components have been optimized to best serve this purpose.

**Our mission:** To enable rich, high-quality RTC applications to be
developed for the browser, mobile platforms, and IoT devices, and allow them
all to communicate via a common set of protocols.

The WebRTC initiative is a project supported by Google, Mozilla and Opera,
amongst others.

### Development

See http://www.webrtc.org/native-code/development for instructions on how to get
started developing with the native code.

[Authoritative list](native-api.md) of directories that contain the
native API header files.

### More info

 * Official web site: http://www.webrtc.org
 * Master source code repo: https://webrtc.googlesource.com/src
 * Samples and reference apps: https://github.com/webrtc
 * Mailing list: http://groups.google.com/group/discuss-webrtc
 * Continuous build: http://build.chromium.org/p/client.webrtc
 * [Coding style guide](style-guide.md)
 * [Code of conduct](CODE_OF_CONDUCT.md)
 ### Build
  # windows
start Developer Command Prompt for VS 2017
cd sourceroot/src
gn gen out/win_x86_vs17_debug --args="target_os=\"win\" target_cpu=\"x86\" is_debug=true rtc_enable_protobuf=false rtc_include_tests=false" --ide=vs2017
gn gen out/win_x86_vs17_release --args="target_os=\"win\" target_cpu=\"x86\" is_debug=false rtc_enable_protobuf=false rtc_include_tests=false symbol_level=0 enable_nacl=false" --ide=vs2017
gn gen out/win_x86_debug --args="target_os=\"win\" target_cpu=\"x86\" is_debug=true rtc_enable_protobuf=false rtc_include_tests=false"
gn gen out/win_x86_release --args="target_os=\"win\" target_cpu=\"x86\" is_debug=false rtc_enable_protobuf=false rtc_include_tests=false symbol_level=0 enable_nacl=false"
gn gen out/win_x64_vs17_debug --args="target_os=\"win\" target_cpu=\"x64\" is_debug=true rtc_enable_protobuf=false rtc_include_tests=false" --ide=vs2017
gn gen out/win_x64_vs17_release --args="target_os=\"win\" target_cpu=\"x64\" is_debug=false rtc_enable_protobuf=false rtc_include_tests=false symbol_level=0 enable_nacl=false" --ide=vs2017
gn gen out/win_x64_debug --args="target_os=\"win\" target_cpu=\"x64\" is_debug=true rtc_enable_protobuf=false rtc_include_tests=false"
gn gen out/win_x64_release --args="target_os=\"win\" target_cpu=\"x64\" is_debug=false rtc_enable_protobuf=false rtc_include_tests=false symbol_level=0 enable_nacl=false"
  # android
gn gen out/android_arm_debug --args='target_os="android" target_cpu="arm" is_debug=true rtc_enable_protobuf=false rtc_include_tests=false'
gn gen out/android_arm_release --args='target_os="android" target_cpu="arm" is_debug=false rtc_enable_protobuf=false rtc_include_tests=false'
  # ios